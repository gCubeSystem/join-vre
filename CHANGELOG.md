
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.7.3] - 2022-01-03

- Make the motivation mandatory when someone asks to join a VRE

## [v3.7.2] - 2021-04-30

- Removed a forgotten sysout in the code :(

## [v3.7.1] - 2020-10-16

- No need to retrieve the user membership request status if the VRE Access policy is Open

## [v3.7.0] - 2020-06-03

Fixes

Bug #19415, Enhance Join VRE portlet performance when many VREs are present on a gateway and the user is logged in.


## [v3.6.1] - [2018-11-09]

Fixed minor glitch border 5px on the body



## [v3.5.0] - [2018-03-13]

Feature #11434, Explore VREs use preloaders while loading VREs list

Ported to GWT 2.8.2



## [v3.5.0] - [2017-11-08]

Added support for optional layout via TabPages and browsing VREs by Organisations and Category



## [v3.4.0] - [2017-07-05]

Ported to GWT 2.8.1

Added support for Terms of Use Display



## [v3.3.0] - [2017-05-15]

Ported to Java8

Added support for template emails



## [v3.2.0] - [2016-11-17]

Removed ASL Session

Implemented Feature #4877 remove VRE association to single Category constraint

Info Button redircets to VRE Public page (if a public page exists for the VRE)



## [v3.1.0] - [2016-10-03]

Added support for invite-only closed groups

Fix for Bug #4877 Email Notification sent multiple times sometimes

Logo VRE was not updated in the portlet due to liferay versioning of Document Library



## [v3.0.0] - [2016-03-28]

Ported to Liferay 6.2



## [v2.0.0] - [2015-10-22]

Refactored to support virtual groups and related descriptions through LR Custom Field

Refactored with GWT Bootstrap, new look and feel

Added support for showing external hosted VREso, see Feature #755



## [v1.1.0] - [2015-04-28]

Redesigned and restyles the way vre description is shown to end users



## [v1.0.0] - [2015-03-02]

First Release