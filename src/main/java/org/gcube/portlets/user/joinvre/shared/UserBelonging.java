package org.gcube.portlets.user.joinvre.shared;

/**
 * @author Massimiliano Assante, ISTI-CNR - massimiliano.assante@isti.cnr.it
 */
public enum UserBelonging { 
	BELONGING, NOT_BELONGING, PENDING
}
